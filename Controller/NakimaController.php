<?php

namespace Nakima\ControllerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class NakimaController extends Controller {

    private $repoMap = array();
    private $request = null;
    private $requestError = false;

    public function loadRequest() {
        if (!$this->request) {
            $this->request = $this->container->get('request_stack')->getCurrentRequest();
        }
        return $this->request;
    }

    public function isPOST() {
        return $this->loadRequest()->getMethod() === "POST";
    }

    public function isGET() {
        return $this->loadRequest()->getMethod() === "GET";
    }

    public function isPUT() {
        return $this->loadRequest()->getMethod() === "PUT";
    }

    public function isDELETE() {
        return $this->loadRequest()->getMethod() === "DELETE";
    }

    public function getParam($name, $error=true) {
        $request = $this->loadRequest();
        $ret = $request->get($name, NULL);
        if (($error) && ($ret === NULL) && !$this->requestError) {
            $this->requestError = $name;
        }
        return $ret;
    }

    public function checkMissingParam() {
        return $this->requestError;
    }

    public function getRepo($fqn) {
        if (!isset($repoMap[$fqn])) {
            $repoMap[$fqn] = $this->getDoctrine()->getRepository($fqn);
        }
        return $repoMap[$fqn];
    }

    public function getRoot() {
        if ($this->isEnvTest()) {
            return getcwd();
        }
        return getcwd() . "/..";
    }

    public function validate($object) {
        $validator = $this->get('validator');
        $errors = $validator->validate($object);

        if (count($errors) > 0) {
            return $errors;
        }

        return false;
    }

    public function getMethod() {
        $request = $this->loadRequest();
        return $request->get("_method", $request->getMethod());
    }

    public function e200JsonResponse($args = array()) {
        return $this->newJsonResponse($args);
    }

    public function newJsonResponse($args = array()) {
        return new JsonResponse(array_merge($args, $this->createBasicParams()));
    }

    /*
     * %field%
     */
    public function e400JsonResponse($field, $message = null) {

        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
            $message = preg_replace("/(.*)(\%field\%)(.*)/i", "$1$field$3", $message);
        } else {
            $domain = "nakima";
            $message = "Parameter '$field' is missing.";
            $localizedMessage = "Parameter '%field%' is missing.";
        }

        $response = $this->newJsonResponse(array(
            "status" => 400,
            "message" => $message,
            "localizedMessage" => $translator->trans(
                "Parameter '%field%' is missing.",
                array(
                    "%field%" => $translator->trans("app.field.$field")
                ),
                $domain),
            "field" => $field,
            "localizedField" => $this->get('translator')->trans("app.field.$field"),
            "code" => 1
        ));

        $response->setStatusCode(400);

        return $response;
    }

    /*
     * %object%
     * %field%
     */
    public function e400FieldInUseJsonResponse($object, $field, $message = null) {
        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
            $message = preg_replace("/(.*)(\%field\%)(.*)/i", "$1$field$3", $message);
            $message = preg_replace("/(.*)(\%object\%)(.*)/i", "$1$object$3", $message);
        } else {
            $domain = "nakima";
            $message = "A $object with this $field is alrady in use.";
            $localizedMessage = "A %object% with this %field% is alrady in use.";
        }

        $response = $this->newJsonResponse(array(
            "status" => 400,
            "message" => $message,
            "localizedMessage" => $translator->trans(
                $localizedMessage,
                array(
                    "%field%" => $translator->trans("app.field.$field"),
                    "%object%" => $translator->trans("app.object.$object")
                ),
                $domain
            ),
            "object" => $object,
            "field" => $field,
            "localizedField" => $this->get('translator')->trans("app.field.$field"),
            "localizedObject" => $this->get('translator')->trans("app.object.$object"),
            "code" => 2
        ));

        $response->setStatusCode(400);

        return $response;
    }

    /*
     * %field%
     */
    public function e400InvalidFieldJsonResponse($field, $message = null) {
        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
            $message = preg_replace("/(.*)(\%field\%)(.*)/i", "$1$field$3", $message);
        } else {
            $domain = "nakima";
            $message = "Invalid field '$field'.";
            $localizedMessage = "Invalid field '%field%'.";
        }

        $response = $this->newJsonResponse(array(
            "status" => 400,
            "message" => $message,
            "localizedMessage" => $translator->trans(
                $localizedMessage,
                array(
                    "%field%" => $translator->trans("app.field.$field")
                ),
                $domain),
            "field" => $field,
            "localizedField" => $this->get('translator')->trans("app.field.$field"),
            "code" => 3
        ));

        $response->setStatusCode(400);

        return $response;
    }

    /*
     */
    public function e401JsonResponse($message = null) {
        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
        } else {
            $domain = "nakima";
            $message = "Access denied.";
            $localizedMessage = $message;
        }

        $response = $this->newJsonResponse(array(
            "status" => 401,
            "message" => $message,
            "localizedMessage" => $translator->trans($localizedMessage, array(), $domain),
            "code" => 4
        ));
        $response->setStatusCode(401);
        return $response;
    }

    /*
     */
    public function e403JsonResponse($message = null) {
        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
        } else {
            $domain = "nakima";
            $message = "You cannot perform this action.";
            $localizedMessage = $message;
        }

        $response = $this->newJsonResponse(array(
            "status" => 403,
            "message" => $message,
            "localizedMessage" => $translator->trans($localizedMessage, array(), $domain),
            "code" => 4
        ));
        $response->setStatusCode(403);
        return $response;
    }

    /*
     * %object%
     * %field%
     */
    public function e404JsonResponse($class, $field, $value, $message = null) {
        $translator = $this->get('translator');

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
            $message = preg_replace("/(.*)(\%field\%)(.*)/i", "$1$field$3", $message);
            $message = preg_replace("/(.*)(\%object\%)(.*)/i", "$1$class$3", $message);
        } else {
            $domain = "nakima";
            $message = "$class with $field $value does not exist.";
            $localizedMessage = "%class% with %field% $value does not exist.";
        }

        $response = $this->newJsonResponse(array(
            "status" => 404,
            "message" => $message,
            "localizedMessage" => $translator->trans(
                $localizedMessage,
                array(
                    "%class%" => $translator->trans("app.object.$class"),
                    "%field%" => $translator->trans("app.field.$field"),
                ),
                $domain
            ),
            "object" => $class,
            "field" => $field,
            "value" => $value,
            "localizedField" => $this->get('translator')->trans("app.field.$field"),
            "localizedObject" => $this->get('translator')->trans("app.object.$class"),
            "code" => 6
        ));
        $response->setStatusCode(404);
        return $response;
    }

    /*
     */
    public function e405JsonResponse($message = null) {
        $translator = $this->get('translator');
        $method = $this->getMethod();

        if ($message) {
            $domain = "messages";
            $localizedMessage = $message;
        } else {
            $domain = "nakima";
            $message = "Method '$method' not allowed.";
            $localizedMessage = $message;
        }

        $response = $this->newJsonResponse(array(
            "status" => 405,
            "message" => $message,
            "localizedMessage" => $translator->trans($localizedMessage, array(), $domain),
            "method" => $method,
            "code" => 7
        ));

        $response->setStatusCode(405);
        return $response;
    }

    public function e500JsonResponse(\Exception $e = null) {
        if ($e == null || $this->container->getParameter('kernel.environment') == "prod") {
            $response = $this->newJsonResponse(array(
                "status" => 500,
                "error" => "Oh fuck! That was not suposed to happen. Look for somebody who is an alfa to solve it."
            ));
        } else {
            $response = array(
                'status' => 500,
                'message' => $e->getMessage(),
                'trace' => array()
            );

            $lines = $e->getTrace();
            foreach ($lines as $key => $line) {
                $l = $this->cleanLine($line);
                if ($l) {
                    $response["trace"][] = $l;
                }
            }

            $response = $this->newJsonResponse($response);
        }

        $response->setStatusCode(500);
        return $response;
    }

    private function cleanLine($line) {
        $pos = isset($line["class"]) &&
                strpos($line["class"], 'Bundle');

        if ($pos) {
            $class = explode("\\", $line["class"]);
            $length = count($class);
            $route = substr($class[0], 0, -6);

            if (!strpos($class[0], "Bundle")) {
                return null;
            }
            $route = $route . ":" . $class[$length - 1];
            $route = $route . ":" . $line["function"];
            if (isset($line["line"])) {
                $route = $route . ":" . $line["line"];
            }
            return $route;
        }
        return null;
    }

    private function createBasicParams() {
        $request = $this->loadRequest();
        $ret = array();
        //debug_print_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 10);

        if ($this->container && "prod" != $this->container->getParameter("kernel.environment")) {

            $routeName = $request->get('_route');
           // var_dump($routeName);

            $matches = array();
            $controller = $request->attributes->get('_controller');
            preg_match('/(.*)Bundle\\\Controller\\\(.*)Controller::(.*)Action/', $controller, $matches);

            /*$ret["server"] = $_SERVER;
            $ret["session"] = $_SESSION;
            $ret["cookie"] = $_COOKIE;
            $ret["request"] = $_REQUEST;
            $ret["request"] = $_REQUEST;
            $ret["get"] = $_GET;
            $ret["post"] = $_POST;*/

            try {
                $ret["symfony"] = array(
                    "route" => $routeName,
                    "uri" => $request->getUri(),
                    "bundle" => $matches[1],
                    "controller" => $matches[2],
                    "action" => $matches[3]
                );
            } catch (\Exception $e) {
                $ret["symfony"] = array(
                    "error" => "error"
                );
            }

            /*$ret["server"] = $_SERVER;
            $ret["session"] = $_SESSION;
            $ret["cookie"] = $_COOKIE;
            $ret["request"] = $_REQUEST;
            $ret["request"] = $_REQUEST;
            $ret["get"] = $_GET;
            $ret["post"] = $_POST;*/
        }

        return $ret;
    }

    public function rawConnection() {
        $host = $this->getParameter('database_host');
        $port = $this->getParameter('database_port');
        $db = $this->getParameter('database_name');
        $user = $this->getParameter('database_user');
        $pass = $this->getParameter('database_password');

        if ($port) {
            $conn = new \PDO("mysql:host=$host;dbname=$db;port=$port;charset=utf8", $user, $pass);
        } else {
            $conn = new \PDO("mysql:host=$host;dbname=$db;charset=utf8", $user, $pass);
        }

        return $conn;
    }

    public function getRemoteIp() {
        if (isset($_SERVER["HTTP_X_REAL_IP"])) {
            return $_SERVER["HTTP_X_REAL_IP"];
        }
        return $_SERVER["REMOTE_ADDR"];
    }

    public function checkEnv($env) {
        return $this->container->getParameter('kernel.environment') === $env;
    }

    public function isEnvTest() {
        return $this->checkEnv("test");
    }

    public function isEnvDev() {
        return $this->checkEnv("dev");
    }

    public function isEnvProd() {
        return $this->checkEnv("prod");
    }

    public function getContainer() {
        return $this->container;
    }

    public function get($param) {
        return parent::get($param);
    }

    public function getParameter($param) {
        return parent::getParameter($param);
    }

    public function getDoctrine() {
        return parent::getDoctrine();
    }
}

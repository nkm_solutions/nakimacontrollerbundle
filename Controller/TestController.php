<?php

namespace Nakima\ControllerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Nakima\ControllerBundle\Controller\NakimaController;
use Nakima\ControllerBundle\Utils\Strings;
use Nakima\ControllerBundle\Utils\Image;

/**
 * @Route("/test")
 */
class TestController extends NakimaController {

    /**
     * @Route("/uploadImage")
     * @Template()
     */
    public function uploadImageAction(Request $request) {
        $file = $request->files->get("image");

        $image = new Image($file);
        //$image = $image->resizeImage(400, 400, true);

        $root = $this->getRoot();

        $image->upload("$root/uploads", "image." . $image->getExtension(), true);

        return $this->newJsonResponse($request, array());
    }

    /**
     * @Route("/convertImage")
     * @Template()
     */
    public function convertImageAction(Request $request) {
        $file = $request->files->get("image");

        $image = new Image($file);
        $image = $image->resizeImage(400, 400, true)->transformToJpeg();

        $root = $this->getRoot();

        $image->upload("$root/uploads", Strings::rstr(8) . $image->getExtension(), true);

        return $this->newJsonResponse($request, array());
    }

    /**
     * @Route("/e401")
     * @Template()
     */
    public function e401Action(Request $request) {
        return $this->e401JsonResponse();
    }

    public function normalAction(Request $request) {
        $this->e200JsonResponse(array());
        $this->e401JsonResponse();
        $this->e403JsonResponse();
        $this->e404JsonResponse("class", "field", "value");
        $this->e405JsonResponse();
        $this->e500JsonResponse(new \Exception);
        $this->getMethod();
        $this->getRemoteIp();
    }
}
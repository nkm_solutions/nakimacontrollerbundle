<?php

namespace Nakima\ControllerBundle\Entity;

/*
 * ORM
 */
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Table;

/*
 * Assert
 * http://symfony.com/doc/current/book/validation.html
 */
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @MappedSuperclass(repositoryClass="Nakima\ControllerBundle\FacebookProfileRepository")
 */
abstract class FacebookProfile {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=256)
     * @Assert\Length(min=32, max=256)
     * @Assert\NotBlank()
     */
    protected $oauthAccessToken;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(min=1, max=64)
     * @Assert\NotBlank()
     */
    protected $facebookName;

    /**
     * @Column(type="integer")
     * @Assert\NotNull()
     */
    protected $facebookId;

    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $user;

    public function __construct() {}

    public function getId() {
        return $this->id;
    }

    public function setId() {
        $this->id = $id;
        return $this;
    }

    public function getOauthAccessToken() {
        return $this->oauthAccessToken;
    }

    public function setOauthAccessToken($oauthAccessToken) {
        $this->oauthAccessToken = $oauthAccessToken;
        return $this;
    }

    public function getFacebookName() {
        return $this->facebookName;
    }

    public function setFacebookName($facebookName) {
        $this->facebookName = $facebookName;
        return $this;
    }

    public function getFacebookId() {
        return $this->facebookId;
    }

    public function setFacebookId($facebookId) {
        $this->facebookId = $facebookId;
        return $this;
    }

    public function getUser() : User {
        return $this->user;
    }

    public function setUser(User $user) {
        $this->user = $user;
        return $this;
    }
}
<?php

namespace Nakima\ControllerBundle\Entity;

/*
 * ORM
 */
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\HasLifecycleCallbacks;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\PostRemove;
use Doctrine\ORM\Mapping\PreRemove;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Table;


/*
 * Assert
 * http://symfony.com/doc/current/book/validation.html
 */
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Nakima\ControllerBundle\Utils\Image;


/**
 * @MappedSuperclass
 * @HasLifecycleCallbacks()
 */
class ImageFile {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="integer")
     */
    protected $size;

    /**
     * @Column(type="string", length=16)
     */
    protected $type;

    /**
     * @Column(type="string", length=256)
     */
    protected $alt;

    /**
     * @Column(type="string", length=256)
     */
    protected $path;

    /**
     * @Column(type="string", length=256)
     */
    protected $dir;

    public function __construct() {}

    public function upload($file, $folder, $filename, $dir=null) {

        $maxX = 1280;
        $maxY = 720;

        if (!$dir) {
            $dir = $folder;
        }

        $dir = getcwd() . "/$folder";

        $image = new Image($file);

        $ratio = $image->getSize()[0] / $image->getSize()[1];
        $ratio2 = $maxX / $maxY;

        // comprimir a lo ancho
        if ($ratio > $ratio2) {
            if ($image->getSize()[0] > $maxX) {
                $image = $image->resizeImage(1280, ($maxX / $image->getSize()[0]) * $image->getSize()[1], true);
            }
        } else {
            // comprimit a lo alto
            if ($image->getSize()[1] > $maxY) {
                $image = $image->resizeImage(($maxY / $image->getSize()[1]) * $image->getSize()[0], 720, true);
            }
        }

        if (!$image->isImage()) {
            return false;
        }

        $this->type = $image->getMimeType();
        $this->path = $folder . "/$filename";
        $this->dir = $dir. "/$filename";
        $this->size = $file->getClientSize();

        $image->upload($dir, $filename, true);

        return $this;
    }

    /**
     * @PreRemove
     */
    public function deleteFile() {
        unlink($this->dir);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getSize() {
        return $this->size;
    }

    public function setSize($size) {
        $this->size = $size;
        return $this;
    }

    public function getPath() {
        return $this->path;
    }

    public function setPath($path) {
        $this->path = $path;
        return $this;
    }

    public function getDir() {
        return $this->dir;
    }

    public function setDir($dir) {
        $this->dir = $dir;
        return $this;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
        return $this;
    }

    public function getFile() {
        return null;
    }

    public function getAlt() {
        return $this->alt;
    }

    public function setAlt($alt) {
        $this->alt = $alt;
        return $this;
    }

    public function setFile($file) {}
}
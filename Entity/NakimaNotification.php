<?php
namespace Nakima\ControllerBundle\Entity;

/*
 * ORM
 */
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\JoinColumn;

/*
 * Assert
 */
use Symfony\Component\Validator\Constraints AS Assert;

/*
 * @MappedSuperclass()
 */
abstract class NakimaNotification {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @Column(type="integer")
     */
    protected $sender;

    /**
     * @Column(type="integer")
     */
    protected $referer;

    /**
     * @Column(type="datetime")
     * @Assert\DateTime()
     */
    protected $createdAt;

    /**
     * @Column(type="boolean")
     * @Assert\Type(
     *     type="bool"
     * )
     */
    protected $alreadyRead;


    /**************************************************************************
     * Resource Functions                                                     *
     **************************************************************************/

    /**
     * @Column(type="integer")
     */
    protected $resource;

    public function getResource() {
        return $this->resource;
    }

    public function setResource($resource) {
        $this->resource = $resource;
        return $this;
    }


    /**************************************************************************
     * Public Functions                                                       *
     **************************************************************************/

    public function __construct() {
        $this->setCreatedAt(new \DateTime);
        $this->alreadyRead = false;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set sender
     *
     * @param integer $sender
     *
     * @return NakimaNotification
     */
    public function setSender($sender) {
        $this->sender = $sender;
        return $this;
    }

    /**
     * Get sender
     *
     * @return integer
     */
    public function getSender() {
        return $this->sender;
    }

    /**
     * Set referer
     *
     * @param integer $referer
     *
     * @return NakimaNotification
     */
    public function setReferer($referer) {
        $this->referer = $referer;
        return $this;
    }

    /**
     * Get referer
     *
     * @return integer
     */
    public function getReferer() {
        return $this->referer;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return NakimaNotification
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * Set alreadyRead
     *
     * @param boolean $alreadyRead
     *
     * @return NakimaNotification
     */
    public function setAlreadyRead($alreadyRead = true) {
        $this->alreadyRead = $alreadyRead;
        return $this;
    }

    /**
     * Get alreadyRead
     *
     * @return boolean
     */
    public function getAlreadyRead() {
        return $this->alreadyRead;
    }
}

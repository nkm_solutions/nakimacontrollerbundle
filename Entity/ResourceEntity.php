<?php
namespace Nakima\ControllerBundle\Entity;

abstract class ResourceEntity {

    public abstract function getJson();

}

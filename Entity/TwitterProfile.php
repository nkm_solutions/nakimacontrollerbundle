<?php

namespace Nakima\ControllerBundle\Entity;

/*
 * ORM
 */
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Table;

/*
 * Assert
 * http://symfony.com/doc/current/book/validation.html
 */
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @MappedSuperclass(repositoryClass="Nakima\ControllerBundle\TwitterProfileRepository")
 */
abstract class TwitterProfile {

    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(min=32, max=64)
     * @Assert\NotBlank()
     */
    protected $oauthAccessToken;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(min=32, max=64)
     * @Assert\NotBlank()
     */
    protected $oauthAccessTokenSecret;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(min=1, max=64)
     * @Assert\NotBlank()
     */
    protected $twitterName;

    /**
     * @Column(type="integer")
     * @Assert\NotNull()
     */
    protected $twitterId;

    /**
     * @OneToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotNull()
     */
    protected $user;

    public function __construct() {}

    public function getId() {
        return $this->id;
    }

    public function setId() {
        $this->id = $id;
        return $this;
    }

    public function getOauthAccessToken() {
        return $this->oauthAccessToken;
    }

    public function setOauthAccessToken($oauthAccessToken) {
        $this->oauthAccessToken = $oauthAccessToken;
        return $this;
    }

    public function getOauthAccessTokenSecret() {
        return $this->oauthAccessTokenSecret;
    }

    public function setOauthAccessTokenSecret($oauthAccessTokenSecret) {
        $this->oauthAccessTokenSecret = $oauthAccessTokenSecret;
        return $this;
    }

    public function getTwitterName() {
        return $this->twitterName;
    }

    public function setTwitterName($twitterName) {
        $this->twitterName = $twitterName;
        return $this;
    }

    public function getTwitterId() {
        return $this->twitterId;
    }

    public function setTwitterId($twitterId) {
        $this->twitterId = $twitterId;
        return $this;
    }

    public function getUser() : User {
        return $this->user;
    }

    public function setUser(User $user) {
        $this->user = $user;
        return $this;
    }
}
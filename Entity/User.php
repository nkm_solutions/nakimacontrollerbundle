<?php

namespace Nakima\ControllerBundle\Entity;

/*
 * ORM
 */
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InheritanceType;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\MappedSuperclass;
use Doctrine\ORM\Mapping\Table;

/*
 * Assert
 * http://symfony.com/doc/current/book/validation.html
 */
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

use Nakima\ControllerBundle\Utils\Strings;
/**
 * @MappedSuperclass(repositoryClass="Nakima\ControllerBundle\Respository\UserRepository")
 * @DoctrineAssert\UniqueEntity("email")
 * @DoctrineAssert\UniqueEntity("username")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    protected $id;

    /**
     * @Column(type="string", length=20, unique=true)
     * @Assert\Length(min=4,max=20)
     * @Assert\NotNull()
     */
    protected $username;

    /**
     * @Column(type="string", length=64)
     * @Assert\Length(max=64)
     * @Assert\NotNull()
     */
    protected $password;

    /**
     * @Column(type="string", length=254, unique=true)
     * @Assert\Email(
     *         checkMX=true,
     *         checkHost=true
     * )
     * @Assert\NotNull()
     */
    protected $email;

    /**
     * @Column(type="datetime", nullable=false)
     * @Assert\DateTime()
     * @Assert\NotNull()
     */
    protected $createdAt;

    /**
     * @Column(type="string", length=64, nullable=true, unique=true)
     * @Assert\Length(min=64, max=64)
     */
    protected $recoverPasswordToken;

    /**
     * @Column(type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $recoverPasswordTokenCreatedAt;

    /**
     * @Column(type="string", length=64, nullable=true, unique=true)
     * @Assert\Length(min=64, max=64)
     */
    protected $registerToken;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $enabled;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $deleted;

    /**
     * @Column(type="boolean", nullable=false)
     */
    protected $isAdmin;

    public function __construct() {
        $this->setEnabled(false);
        $this->setDeleted(false);
        $this->setIsAdmin(false);
        $this->setCreatedAt(new \DateTime);
    }

    public function getId() {
        return $this->id;
    }

    public function setUsername($username) {
        $this->username = $username;

        return $this;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getRoles() {
        if ($this->getIsAdmin()) {
            return array('ROLE_ADMIN');
        }
        return array('ROLE_USER');
    }

    public function getSalt() {
        return null;
    }

    public function eraseCredentials() {
    }

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt() {
        return $this->createdAt;
    }

    public function setRecoverPasswordToken($recoverPasswordToken) {
        $this->recoverPasswordToken = $recoverPasswordToken;
        $this->setRecoverPasswordTokenCreatedAt(new \DateTime);

        return $this;
    }

    public function getRecoverPasswordToken() {
        return $this->recoverPasswordToken;
    }

    public function setRecoverPasswordTokenCreatedAt($recoverPasswordTokenCreatedAt) {
        $this->recoverPasswordTokenCreatedAt = $recoverPasswordTokenCreatedAt;

        return $this;
    }

    public function getRecoverPasswordTokenCreatedAt() {
        return $this->recoverPasswordTokenCreatedAt;
    }

    public function setEnabled($enabled) {
        $this->enabled = $enabled;

        return $this;
    }

    public function getEnabled() {
        return $this->enabled;
    }

    public function delete() {
        $crap = Strings::rstr(20);
        $this->setEnabled(false);
        $this->setEmail("$crap@anymore.com");
        $this->setPassword($crap);
        $this->setUsername($crap);
        $this->setDeleted(true);
    }

    public function setRegisterToken($registerToken) {
        $this->registerToken = $registerToken;

        return $this;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getRegisterToken() {
        return $this->registerToken;
    }

    public function getDeleted() {
        return $this->deleted;
    }

    public function setDeleted($deleted) {
        $this->deleted = $deleted;
        return $this;
    }

    public function getIsAdmin() {
        return $this->isAdmin;
    }

    public function setIsAdmin($isAdmin) {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    // @see \Serializable::unserialize()
    public function unserialize($serialized) {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
}

# Instalation

composer require joshtronic/php-loremipsum
composer require vivi/controller-bundle ^1.2


# Namespaces

[Nakima\ControllerBundle\Controller](./docs/Controller/index.md)  
[Nakima\ControllerBundle\Repository](./docs/Repository/index.md)  
[Nakima\ControllerBundle\Utils](./docs/Utils/index.md)  
[Nakima\ControllerBundle\Tests\Controller](./docs/Tests/Controller/index.md)  
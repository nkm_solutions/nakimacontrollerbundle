<?php

namespace Nakima\ControllerBundle\Repository;

use Nakima\ControllerBundle\Repository\NakimaRepository;

class FacebookProfileRepository extends NakimaRepository {

    protected function transform($facebookProfile, $translator = null, $format = 'json') {

        return array(
            "id" => $facebookProfile->getId()
        );
    }


}
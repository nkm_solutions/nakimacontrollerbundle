<?php

namespace Nakima\ControllerBundle\Repository;

abstract class NakimaNotificationRepository extends NakimaRepository {

    protected function transform($notification, $translator=null, $format='json') {

        return array(
            "id" => $notification->getId(),
            "sender" => $notification->getSender(),
            "referer" => $notification->getReferer(),
            "createdAt" => $notification->getCreatedAt(),
            "alreadyRead" => $notification->getAlreadyRead(),
            "resource" => $notification->getResource()->getJson()
        );
    }

    public function fetchByReferer($referer_id) {
        return $this->findBy(
            array('referer' => $referer_id),
            array('createdAt' => 'DESC')
        );
    }

    public abstract function fetchUsersInvolved($connection, $resource);

}

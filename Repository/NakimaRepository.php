<?php

namespace Nakima\ControllerBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\PersistentCollection;


abstract class NakimaRepository extends EntityRepository {

    protected abstract function transform($entity, $translator = null, $format = 'json');

    public function transformEntity($entity, $translator = null, $format = 'json') {
        if (!$entity) {
            return null;
        }

        if (
            $entity instanceof ArrayCollection or
            $entity instanceof PersistentCollection or
            is_array($entity)
        ) {

            $ret = array();

            foreach ($entity as $key => $value) {
                $ret[] = $this->transform($value, $translator, $format);
            }

            return $ret;
        } else {
            return $this->transform($entity, $translator, $format);
        }
    }

    protected function getRepo($fqn) {
        return $this->getEntityManager()
                    ->getRepository($fqn);
    }
}
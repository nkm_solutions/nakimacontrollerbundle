<?php

namespace Nakima\ControllerBundle\Repository;

use Nakima\ControllerBundle\Repository\NakimaRepository;

class TwitterProfileRepository extends NakimaRepository {

    protected function transform($twitterProfile, $translator = null, $format = 'json') {

        return array(
            "id" => $twitterProfile->getId()
        );
    }


}
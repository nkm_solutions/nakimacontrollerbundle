<?php

namespace Nakima\ControllerBundle\Repository;

class UserRepository extends NakimaRepository {

    protected function transform($user, $translator = null, $format = 'json') {

        return array(
            "id" => $user->getId(),
            "username" => $user->getUsername(),
            "email" => $user->getEmail(),
            "createdAt" => $user->getCreatedAt()
        );
    }

}
<?php

namespace Nakima\ControllerBundle\Tests\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use joshtronic\LoremIpsum;

abstract class NakimaApiControllerTestCase extends WebTestCase
{

    private $clientQueue = array();
    private $userQueue = array();
    private $json = null;
    private $lorem;

    protected $client = null;
    protected $user = null;

    private $isDummy = false;
    private static $dummiesClient = array();
    private static $dummiesUser = array();

    /**
     * @before
     */
    public function clean() {
        $this->lorem = new LoremIpsum;
        $this->clientQueue = array();
        $this->client = null;
        $this->user = null;
        $this->json = null;
    }

    protected function newUser($followRedirects = true, $info = array()) {

        if ($this->client && !$this->isDummy) {
            $this->clientQueue[] = $this->client;
            $this->userQueue[] = $this->user;
        }

        $this->isDummy = false;

        if (isset($info["dummy"]) && $info["dummy"]) {
            if (!isset($info["alias"])) {
                $info["alias"] = "dummy";
            }

            $this->isDummy = true;

            if (!isset(self::$dummiesClient[$info["alias"]])) {
                $this->client = static::createClient();
                $this->client->followRedirects();
                self::$dummiesClient[$info["alias"]] = $this->client;
                $username = $this->registerUser($info);
                $this->user = $this->loginUser($username);
                self::$dummiesUser[$info["alias"]] = $this->user;
            } else {
                $this->client = self::$dummiesClient[$info["alias"]];
                $this->user = self::$dummiesUser[$info["alias"]];
            }

            return $this->user;

        } else {
            $this->client = static::createClient();
            if ($followRedirects) {
                $this->client->followRedirects();
            }

            $username = $this->registerUser($info);

            $this->user = $this->loginUser($username);
            return $this->user;
        }
    }

    protected function newAnonymous($followRedirects = true) {

        if ($this->client) {
            $this->clientQueue[] = $this->client;
            $this->userQueue[] = $this->user;
        }

        $this->client = static::createClient();
        if ($followRedirects) {
            $this->client->followRedirects();
        }

        $this->user = null;
        return $this->client;
    }

    protected function popUser($save = false) {
        if (count($this->clientQueue) == 0) {
            return null;
        }
        $tmp = $this->client;
        $tmpUser = $this->user;

        $this->client = array_pop($this->clientQueue);
        $this->user = array_pop($this->userQueue);
        if ($save) {
            $this->clientQueue[] = $tmp;
            $this->userQueue[] = $tmpUser;
        }
        $tmp = null;
        $tmpUser = null;
        return $this->client;
    }

    protected function getClient() {
        return $this->client;
    }

    protected function getUser() {
        return $this->user;
    }

    /**
     * @return {String} the new user's username
     */
    protected function getStatusCode() {
        if (!$this->client) {
            return -1;
        }

        if (!$this->client->getResponse()) {
            return -1;
        }

        return $this->client->getResponse()->getStatusCode();
    }

    protected function check200() {
        if ($this->getStatusCode() === 500) {
            $this->printJson();
        }
        if (200 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(200, $this->getStatusCode());
    }

    protected function check400() {
        if ($this->getStatusCode() === 500) {
            $this->printJson();
        }
        if (400 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(400, $this->getStatusCode());
        $this->assertObjectHasAttribute("status", $this->getJson());
        //$this->assertObjectHasAttribute("code", $this->getJson());
        $this->assertObjectHasAttribute("message", $this->getJson());
        //$this->assertObjectHasAttribute("localizedMessage", $this->getJson());
        //$this->assertObjectHasAttribute("field", $this->getJson());
        //$this->assertObjectHasAttribute("localizedField", $this->getJson());
    }

    protected function check401() {
        if ($this->getStatusCode() === 500) {
            $this->printJson();
        }
        if (401 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(401, $this->getStatusCode());
        $this->assertObjectHasAttribute("status", $this->getJson());
        $this->assertObjectHasAttribute("message", $this->getJson());
    }

    protected function check403() {
        if (403 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(403, $this->getStatusCode());
        $this->assertObjectHasAttribute("status", $this->getJson());
        $this->assertObjectHasAttribute("message", $this->getJson());
    }

    protected function check404() {
        if (404 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(404, $this->getStatusCode());
        $this->assertObjectHasAttribute("status", $this->getJson());
        $this->assertObjectHasAttribute("message", $this->getJson());
    }

    protected function check405() {
        if (405 != $this->getStatusCode()) {
            $this->printJson();
        }
        $this->assertEquals(405, $this->getStatusCode());
        $this->assertObjectHasAttribute("status", $this->getJson());
        $this->assertObjectHasAttribute("message", $this->getJson());
    }

    protected function printRaw() {
        print_r($this->client->getResponse()->getContent());
    }

    protected function printJson() {
        $this->getJson();
        if (json_last_error() != JSON_ERROR_NONE) {
            echo "\n";
            $dom = new \DOMDocument;
            $dom->loadHTML($this->client->getResponse()->getContent());
            $title = $dom->getElementsByTagName('title')[0];
            $title = $title->textContent;
            echo "\033[1;31m$title:\033[0m";
            echo "\n";
        } else {
            echo "\n";
            $this->printVar("Response", $this->getJson());
        }
    }

    protected function getJson() {
        if (!$this->client) {
            return null;
        }

        if (!$this->client->getResponse()) {
            return null;
        }

        if (!$this->json) {
            $this->json = json_decode($this->client->getResponse()->getContent());
        }
        return $this->json;
    }

    protected function get($url, $params = array(), $files = array()) {
        return $this->request("GET", $url, $params, $files);
    }

    protected function post($url, $params = array(), $files = array()) {
        return $this->request("POST", $url, $params, $files);
    }

    protected function put($url, $params = array(), $files = array()) {
        return $this->request("PUT", $url, $params, $files);
    }

    protected function delete($url, $params = array(), $files = array()) {
        return $this->request("DELETE", $url, $params, $files);
    }

    protected function patch($url, $params = array(), $files = array()) {
        return $this->request("PATCH", $url, $params, $files);
    }

    protected function head($url, $params = array(), $files = array()) {
        return $this->request("HEAD", $url, $params, $files);
    }

    protected function options($url, $params = array(), $files = array()) {
        return $this->request("OPTIONS", $url, $params, $files);
    }

    protected function request($method, $url, $params = array(), $files = array()) {
        $this->json = null;

        if (!$this->client) {
            $this->assertNotNull($this->client, "Client needs to be initialized, must call newAnonymous or newUser.");
        }

        $ret = $this->client->request($method, $url, $params, $files);

        if ($this->getStatusCode() === 500) {
           $this->printJson();
        }
        return $this->getJson();
    }

    protected function createFileParam($folder, $newFile) {

        $folder = getcwd() . "/" . $folder;
        $file = $folder . "/" . $newFile;

        system("cp -f $file /tmp/$newFile");

        return new UploadedFile(
            "/tmp/$newFile",
            $newFile
        );
    }


    protected function refreshUser() {
        return $this->user = $this->refresh();
    }

    protected function rstr($length = 8) {
        $result = null;
        $replace = array('/', '+', '=');
        while(!isset($result[$length-1])) {
            $result.= str_replace($replace, NULL, base64_encode(mcrypt_create_iv($length, MCRYPT_RAND)));
        }
        return substr($result, 0, $length);
    }

    protected function loremWords($length = 0) {
        $this->lorem = new LoremIpsum;
        return $this->lorem->words($length);
    }

    protected function loremSentences($length = 0) {
        $this->lorem = new LoremIpsum;
        return $this->lorem->sentences($length);
    }

    protected function loremParagraphs($length = 0) {
        $this->lorem = new LoremIpsum;
        return $this->lorem->paragraphs($length);
    }

    /**
     * This must register a user in to the system with the given info
     *
     * @return {String} the new user's username
     */
    protected abstract function registerUser($info = array());

    /**
     * This must login a user in to the system with the given info
     */
    protected abstract function loginUser($username);

    /**
     * This must refresh user info
     */
    protected abstract function refresh();

    protected function printVar($var, $value) {
        echo "\n";
        $array = false;
        $dicc = false;

        try {
            $array = is_array($value);
            $dicc = get_class($value) === "stdClass";
        } catch (\Exception $e) {}

        if ($array || $dicc) {
            $this->printJson2($var, $value);
        } else {
            echo "\033[1;37m$var:\033[0m \033[1;33m$value\033[0m\n";
        }
    }

    private function printJson2($var, $json) {

        foreach ($json as $key => $value) {
            $array = false;
            $dicc = false;

            try {
                $array = is_array($value);
                $dicc = get_class($value) === "stdClass";
            } catch (\Exception $e) {}

            if (!($array || $dicc)) {
                if ($value === false) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mfalse\033[0m\n";
                } else if ($value === NULL) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mNULL\033[0m\n";
                } else if ($value === "") {
                    echo "\033[1;37m$key:\033[0m \033[1;33m''\033[0m\n";
                } else if ($value === 0) {
                    echo "\033[1;37m$key:\033[0m \033[1;33m0\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                }
            } else {
                $count = count($value);
                if ($dicc) {
                    echo "\033[1;37m$key:\033[0m \033[1;34mOBJECT\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;34mARRAY ($count)\033[0m\n";
                }
                $this->printJsonLvl($key, 1, $value);
            }
        }
    }

    private function printJsonLvl($var, $lvl, $json) {
        foreach ($json as $key => $value) {
            $array = false;
            $dicc = false;

            try {
                $array = is_array($value);
                $dicc = get_class($value) === "stdClass";
            } catch (\Exception $e) {}

            if ($array || $dicc) {
                $this->printSpaces($lvl);
                $count = count($value);
                if ($dicc) {
                    echo "\033[1;37m$key:\033[0m \033[1;34mOBJECT\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;34mARRAY ($count)\033[0m\n";
                }
                $this->printJsonLvl($key, $lvl + 1, $value);
            } else {
                $this->printSpaces($lvl);
                if (is_string($value)) {
                    if (strlen($value) <= 100) {
                        echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                    } else {
                        echo "\033[1;37m$key:\033[0m \033[1;33m" . substr($value, 0, 100) . "...\033[0m\n";
                    }
                } else if ($value === false) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mfalse\033[0m\n";
                } else if ($value === NULL) {
                    echo "\033[1;37m$key:\033[0m \033[1;33mNULL\033[0m\n";
                } else if ($value === "") {
                    echo "\033[1;37m$key:\033[0m \033[1;33m''\033[0m\n";
                } else if ($value === 0) {
                    echo "\033[1;37m$key:\033[0m \033[1;33m0\033[0m\n";
                } else {
                    echo "\033[1;37m$key:\033[0m \033[1;33m$value\033[0m\n";
                }
            }
        }
    }

    private function printSpaces($lvl) {
        $mult = 2;

        $total = $lvl * $mult;

        for ($i = 0; $i < $total; $i++) {
            echo " ";
        }
    }
}



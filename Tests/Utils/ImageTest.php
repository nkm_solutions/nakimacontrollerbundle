<?php

namespace Nakima\ControllerBundle\Tests\Utils;

class ImageTest extends JoyerApiControllerTestCase
{

    protected function refresh() {}

    protected function loginUser($username) {}

    protected function registerUser($info=array()) {}

    /**
     * @test
     * @author xGonzalez
     */
    public function Image1_200_post_a() {
        $this->newAnonymous();

        $files = array(
            "image" => $this->createFileParam("test_files/", "image.png")
        );

        $this->post("/uploadImage");
        $this->check200();
    }

}
<?php

namespace Nakima\ControllerBundle\Utils;

use Symfony\Component\HttpFoundation\Request;
use Nakima\ControllerBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;
use Facebook;

class FacebookHelper {

    private $controller;
    private $container;
    private $settings;
    private $facebook;
    private $info;

    public function __construct($controller) {
        $this->controller = $controller;
        $this->container = $controller->getContainer();

        $this->settings = [
            'app_id' => $this->container->getParameter('facebook_app_id'),
            'app_secret' => $this->container->getParameter('facebook_app_secret'),
            'default_graph_version' => $this->container->getParameter('facebook_version')
        ];

        /*$this->facebook = new Facebook\Facebook([
            'app_id' => '522506747786501',
            'app_secret' => '1123b47e482864af41ab23d448d8e945',
            'default_graph_version' => 'v2.2',
        ]);*/

        $this->facebook = new Facebook\Facebook($this->settings);
    }

    public function getFacebookLoginUrl() {

        if (!session_id()) {
            session_start();
        }

        $helper = $this->facebook->getRedirectLoginHelper();

        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl('http://localhost:8000/api/facebook/check', $permissions);

        setcookie("FBRLH_state", $_SESSION["FBRLH_state"]);
        $_COOKIE["FBRLH_state"] = $_SESSION["FBRLH_state"];

        return htmlspecialchars($loginUrl);
    }

    public function getFacebookUserCredentials(Request $request) {

        if (!session_id()) {
            session_start();
        }

        $this->facebook = new Facebook\Facebook($this->settings);
        $helper = $this->facebook->getRedirectLoginHelper();
        $_SESSION["FBRLH_state"] = $_COOKIE["FBRLH_state"];

        setcookie("FBRLH_state", "", time()-3600);
        try {
          $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            return false;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            return false;
        }

        if (! isset($accessToken)) {
            return false;
        }

        $oAuth2Client = $this->facebook->getOAuth2Client();

        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        $tokenMetadata->validateAppId($this->container->getParameter('facebook_app_id'));

        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
              } catch (Facebook\Exceptions\FacebookSDKException $e) {
                return false;
              }
        }

        $oResponse = $this->facebook->get('/me?fields=id,name,email', $accessToken->getValue());

        $settings = [
            "user_id" => $oResponse->getGraphUser()["id"],
            "token" => $accessToken->getValue(),
            "screen_name" => $oResponse->getGraphUser()["name"],
            "email" => $oResponse->getGraphUser()["email"]
        ];

        $this->info = $settings;

        return $settings;
    }

    public function loadUser($repo, $firewall) {
        $repo = $this->controller->getRepo($repo);
        $profile = $repo->findOneByFacebookId($this->info['user_id']);

        if (!$profile) {
            return false;
        }

        $profile->setOauthAccessToken($this->info["token"]);

        $manager = $this->controller->getDoctrine()->getManager();
        $manager->persist($profile);
        $manager->flush();

        $user = $profile->getUser();

        if ($user->getEnabled()) {
            $encoder_service = $this->controller->get('security.encoder_factory');
            $encoder         = $encoder_service->getEncoder($user);

            $key     = $this->controller->getParameter('secret'); // your security key from parameters.yml
            $t       = new RememberMeToken($user, $firewall, $key);
            $session = $this->controller->loadRequest()->getSession();
            $session->set($firewall, serialize($t));
            $session->save();
            $this->controller->get('security.token_storage')->setToken($t);
        }

        return $user;
    }
}
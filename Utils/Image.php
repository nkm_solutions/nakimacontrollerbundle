<?php

namespace Nakima\ControllerBundle\Utils;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class Image {

    private $image;
    private $path;
    private $isImage;
    private $info;

    public function __construct($source, $path="") {
        if ($source instanceof UploadedFile) {
            $source = $source->getPathName();
            $this->path = $source;
            $this->image = imagecreatefromstring(file_get_contents($this->path));
        } else if (is_string($source)) {
            $this->path = $source;
            $this->image = imagecreatefromstring(file_get_contents($this->path));
        } else {
            $this->path = $path;
            $this->image = $source;
        }

        if (!$this->image) {
            $this->isImage = false;
        } else {
            $this->isImage = is_array(getimagesize($this->path));
        }


        if ($this->isImage) {
            $this->info = getimagesize($this->path);
        }

        if ($this->isPng()) {
            imagealphablending($this->image, true);
            imagesavealpha($this->image, true);
        }
    }

    private function _isImage($image) {
        if (!$this->image) {
            return false;
        } else {
            return @is_array(getimagesize($this->path));
        }
    }

    public function isImage() {
        return $this->isImage;
    }

    public function getMimeType() {
        return $this->info["mime"];
    }

    public function isPng() {
        return $this->getMimeType() === "image/png";
    }

    public function isJpeg() {
        return $this->getMimeType() === "image/jpeg";
    }

    public function isGif() {
        return $this->getMimeType() === "image/gif";
    }

    public function transformToPng($destroy=false) {
        $path = "/tmp/" . $this->rstr(6) . "png";

        imagepng($this->image, $path);

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function transformToJpeg($destroy=false, $color=array(255,255,255)) {
        $path = "/tmp/" . $this->rstr(6) . "jpg";

        if (!$this->isJpeg) {
            $bg = imagecreatetruecolor(imagesx($this->image), imagesy($this->image));
            imagefill($bg, 0, 0, imagecolorallocate($bg, $color[0], $color[1], $color[2]));
            imagealphablending($bg, TRUE);
            imagecopy($bg, $this->image, 0, 0, 0, 0, imagesx($this->image), imagesy($this->image));
            $quality = 100;
            imagejpeg($bg, $path, $quality);
        }

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function transformToGif($destroy = false) {
        $path = "/tmp/" . $this->rstr(6) . "gif";

        imagegif($this->image, $path);

        if ($destroy) {
            $this->destroy();
        }

        return new Image($path);
    }

    public function getSize() {
        return array(
            $this->info[0],
            $this->info[1]
        );
    }

    public function resizeImage($width, $height, $destroy = false) {

        $extension = ".jpg";
        if ($this->isGif()) {
            $extension = ".gif";
        } else if ($this->isPng()) {
            $extension = ".png";
        }

        $name = $this->rstr(6) . $extension;
        $path = "/tmp/" . $name;
        $resized = imagecreatetruecolor($width, $height);
        imagecopyresized($resized, $this->image, 0, 0, 0, 0, $width, $height, $this->info[0], $this->info[1]);

        if ($this->isGif()) {
            $ret = imagegif($resized, $path);
        } else if ($this->isPng()) {
            $ret = imagepng($resized, $path);
        } else {
            $ret = imagejpeg($resized, $path);
        }


        if ($destroy) {
            $this->destroy();
        }

        return new Image($resized, $path);
    }

    public function cropImage($x, $y, $offsetX, $offsetY, $destroy = false) {

        $extension = ".jpg";
        if ($this->isGif()) {
            $extension = ".gif";
        } else if ($this->isPng()) {
            $extension = ".png";
        }

        $name = $this->rstr(6) . $extension;
        $path = "/tmp/" . $name;

        $cropped = imagecreatetruecolor($x, $y);
    //  imagecopyresampled($dst_image, $src_image  , $dst_x, $dst_y, $src_x  , $src_y  , $dst_w, $dst_h, $src_w, $src_h);
        imagecopyresampled($cropped  , $this->image, 0     , 0     , $offsetX, $offsetY, $x    , $y    , $x    , $y    );

        if ($this->isGif()) {
            $ret = imagegif($cropped, $path);
        } else if ($this->isPng()) {
            $ret = imagepng($cropped, $path);
        } else {
            $ret = imagejpeg($cropped, $path);
        }

        if ($destroy) {
            $this->destroy();
        }

        return new Image($cropped, $path);
    }

    public function fitInto($width, $height, $destroy = true) {
        $size = $this->getSize();

        $w = $size[0];
        $h = $size[1];


        if (($w / $width) > ($h / $height)) {
            $image = $this->resizeImage($height * $w / $h, $height, $destroy);
        } else {
           $image = $this->resizeImage($width, $width * $h / $w, $destroy);
        }

        $size = $image->getSize();

        $w = $size[0];
        $h = $size[1];

        $ox = abs(($w - $width) / 2);
        $oh = abs(($h - $height) / 2);

        $image = $image->cropImage($width, $height, $ox, $oh, $destroy);

        return $image;


    }

    public function destroy() {
        imagedestroy($this->image);
    }

    public function getFile() {
        return $this->image;
    }

    public function getPath() {
        return $this->path;
    }

    public function upload($folder, $file, $destroy = false) {

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        if ($this->isGif()) {
            $ret = imagegif($this->image, "$folder/$file");
        } else if ($this->isPng()) {
            $ret = imagepng($this->image, "$folder/$file");
        } else {
            $ret = imagejpeg($this->image, "$folder/$file");
        }

        if ($destroy) {
            $this->destroy();
        }


        return $ret;
    }

    public function getExtension() {
        if ($this->isPng()) {
            return "png";
        } else if ($this->isGif()) {
            return "gif";
        } else {
            return "jpg";
        }
    }

    private function rstr($length) {
        $result = null;
        $replace = array('/', '+', '=');
        while(!isset($result[$length-1])) {
            $result.= str_replace($replace, NULL, base64_encode(mcrypt_create_iv($length, MCRYPT_RAND)));
        }
        return substr($result, 0, $length);
    }
}
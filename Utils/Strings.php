<?php

namespace Nakima\ControllerBundle\Utils;

class Strings {

    public static function rstr($length) {
        $result = null;
        $replace = array('/', '+', '=');
        while(!isset($result[$length-1])) {
            $result.= str_replace($replace, NULL, base64_encode(mcrypt_create_iv($length, MCRYPT_RAND)));
        }
        return substr($result, 0, $length);
    }

    public static function startsWith($haystack, $needle) {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }
}

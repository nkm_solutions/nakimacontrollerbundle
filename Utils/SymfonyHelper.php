<?php

namespace Nakima\ControllerBundle\Utils;

class SymfonyHelper {

    public static function getRoot() {
        $dir = getcwd();
        return preg_replace("/(.*)(\/web.*)/", "$1", $dir);
    }
}
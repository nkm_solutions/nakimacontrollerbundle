<?php

namespace Nakima\ControllerBundle\Utils;

class Time {

    public $datetime;

    public function __construct($datetime) {
        $this->datetime = $datetime;
    }

    public static function byDate($year, $month, $day) {
        if ($month < 10) {
            $month = "0$month";
        }

        if ($day < 10) {
            $day = "0$day";
        }

        $ret = new Time(new \DateTime("$year-$month-$day"));

        $this->datetime("$year-$month-$day");
    }

    public static function now() {
        return new Time(new \DateTime);
    }

    public static function byDateTime($year, $month, $day, $hours, $minutes, $seconds) {
        if ($month < 10) {
            $month = "0$month";
        }

        if ($day < 10) {
            $day = "0$day";
        }

        if ($hours < 10) {
            $hours = "0$hours";
        }

        if ($minutes < 10) {
            $minutes = "0$minutes";
        }

        if ($seconds < 10) {
            $seconds = "0$seconds";
        }

        $ret = new Time(new \DateTime("$year-$month-$day $hours:$minutes:$seconds"));

        $this->datetime("$year-$month-$day");
    }

    public function __toString() {
        return (string) $this->datetime;
    }

    public function getWeekNumber() {
        return $this->datetime->format("W");
    }

    public function getDayOfTheYear() {
        return $this->datetime->format("z");
    }

    public function getYear() {
        return $this->datetime->format("Y");
    }

    public function addSeconds() {
        throw new \Exception("TODO");
    }

    public function addMinutes() {
        throw new \Exception("TODO");
    }

    public function addHours() {
        throw new \Exception("TODO");
    }

    public function addDays() {
        throw new \Exception("TODO");
    }

    public function addWeeks() {
        throw new \Exception("TODO");
    }

    public function addMonths() {
        throw new \Exception("TODO");
    }

    public function addYears() {
        throw new \Exception("TODO");
    }

    public function substractSeconds() {
        throw new \Exception("TODO");
    }

    public function substractMinutes() {
        throw new \Exception("TODO");
    }

    public function substractHours() {
        throw new \Exception("TODO");
    }

    public function substractDays() {
        throw new \Exception("TODO");
    }

    public function substractWeeks() {
        throw new \Exception("TODO");
    }

    public function substractMonths() {
        throw new \Exception("TODO");
    }

    public function substractYears() {
        throw new \Exception("TODO");
    }
}
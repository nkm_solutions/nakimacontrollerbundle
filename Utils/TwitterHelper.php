<?php

namespace Nakima\ControllerBundle\Utils;

use Symfony\Component\HttpFoundation\Request;
use TwitterAPIExchange;
use Nakima\ControllerBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\RememberMeToken;

class TwitterHelper {

    private $controller;
    private $container;
    private $settings;
    private $twitter;
    private $info;

    public function __construct($controller) {
        $this->controller = $controller;
        $this->container = $controller->getContainer();

        $this->settings = array(
            'oauth_access_token' => "",
            'oauth_access_token_secret' => "",
            'consumer_key' => $this->container->getParameter('twitter_consumer_key'),
            'consumer_secret' => $this->container->getParameter('twitter_consumer_secret')
        );
    }

    public function getTwitterApi() {
        return $this->twitter;
    }

    public function getTwitterToken() {

        $this->twitter = new TwitterAPIExchange($this->settings);

        $resp = $this->get('https://api.twitter.com/oauth/request_token');

        $resp = $this->cleanRawResponse($resp);

        return $resp["oauth_token"];
    }

    public function getTwitterUserCredentials(Request $request) {

        $this->twitter = new TwitterAPIExchange($this->settings);

        $oauth = $request->get("oauth_token");
        $verify = $request->get("oauth_verifier");

        $this->settings["oauth_access_token"] = $oauth;

        $this->twitter = new TwitterAPIExchange($this->settings);
        $resp = $this->post('https://api.twitter.com/oauth/access_token', array(
            "oauth_verifier" => $verify
        ));
        $resp = $this->cleanRawResponse($resp);
        $settings = array(
            'consumer_key' => $this->container->getParameter('twitter_consumer_key'),
            'consumer_secret' => $this->container->getParameter('twitter_consumer_secret'),
            'screen_name' => $resp["screen_name"],
            'user_id' => $resp['user_id'],
            'oauth_access_token' => $resp["oauth_token"],
            'oauth_access_token_secret' => $resp["oauth_token_secret"]

        );

        $this->info = $settings;
        return $settings;
        /*$settings = $this->getSettings();
        $settings["oauth_access_token"] = $params["oauth_token"];
        $settings["oauth_access_token_secret"] = $params["oauth_token_secret"];

        $twitter = new TwitterAPIExchange($settings);
        $token = $twitter->buildOauth('https://api.twitter.com/1.1/friends/ids.json', 'GET')
            //->setPostfields($postfields)
            ->performRequest();
        echo($token);*/
    }

    public function getTwitterFromCredentials($settings) {
        $this->settings = array(
            'oauth_access_token' => $settings["oauth_token"],
            'oauth_access_token_secret' => $settings["oauth_token_secret"],
            'consumer_key' => $this->container->getParameter('twitter_consumer_key'),
            'consumer_secret' => $this->container->getParameter('twitter_consumer_secret')
        );

        $this->twitter = new TwitterAPIExchange($this->settings);
    }

    public function cleanRawResponse($resp) {
        $paramList = explode("&", $resp);
        $params = array();
        foreach ($paramList as $key => $value) {
            $ps = explode("=", $value);
            $params[$ps[0]] = $ps[1];
        }

        return $params;
    }

    public function get($url, $params = array()) {
        $query = http_build_query($params);

        $token = $this->twitter->buildOauth($url, 'GET')
            ->setGetfield($query)
            ->performRequest();

        return $token;
    }

    public function post($url, $params = array()) {

        $token = $this->twitter->buildOauth($url, 'POST')
            ->setPostfields($params)
            ->performRequest();

        return $token;
    }

    public function loadUser($repo, $firewall) {
        $repo = $this->controller->getRepo($repo);
        $profile = $repo->findOneByTwitterId($this->info['user_id']);

        if (!$profile) {
            return false;
        }

        $profile->setOauthAccessToken($this->info["oauth_access_token"]);
        $profile->setOauthAccessTokenSecret($this->info["oauth_access_token_secret"]);

        $manager = $this->controller->getDoctrine()->getManager();
        $manager->persist($profile);
        $manager->flush();

        $user = $profile->getUser();

        $encoder_service = $this->controller->get('security.encoder_factory');
        $encoder         = $encoder_service->getEncoder($user);

        $key     = $this->controller->getParameter('secret'); // your security key from parameters.yml
        $t       = new RememberMeToken($user, $firewall, $key);
        $session = $this->controller->loadRequest()->getSession();
        $session->set($firewall, serialize($t));
        $session->save();
        $this->controller->get('security.token_storage')->setToken($t);

        return $user;
    }
}
# Nakima\ControllerBundle\Controller\NakimaController

## Methods

|           |              |                                                               |  
| --------- | ------------ | ------------------------------------------------------------- |  
| protected | Repository   | **getRepo**(String)                                           |  
| protected | JsonResponse | **newJsonResponse**(Request, [args])                          |  
| protected | JsonResponse | **e400JsonResponse**(Request, $field, [message])              |  
| protected | JsonResponse | **e401JsonResponse**(Request, [message])                      |  
| protected | JsonResponse | **e403JsonResponse**(Request, [message])                      |  
| protected | JsonResponse | **e404JsonResponse**(Request, class, field, value, [message]) |  
| protected | JsonResponse | **e405JsonResponse**(Request, [message])                      |  
| protected | JsonResponse | **e500JsonResponse**(Request, [Exception])                    |  
| protected | PDO          | **rawConnection**()                                           |  
| protected | String       | **getRemoteIp**()                                             |  
| protected | boolean      | **checkEnv**(env)                                             |  
| protected | boolean      | **checkEnvTest**()                                            |  
| protected | boolean      | **checkEnvProd**()                                            |  
| protected | boolean      | **checkEnvDev**()                                             |  

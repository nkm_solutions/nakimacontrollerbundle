# Nakima\ControllerBundle\Repository\NakimaRepository

## Methods

|             |            |                                                     |  
| ----------- | ---------- | --------------------------------------------------- |  
| _protected_ | array      | **transform**(Entity, Translator, $format)          |  
| public      | array      | **transformEntity**(Entity, Translator, $format)    |  
| public      | array      | **transformEntity**(`*`Entity, Translator, $format) |  
| protected   | Repository | **getRepo**(String)                                 |  
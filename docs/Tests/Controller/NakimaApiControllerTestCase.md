# Nakima\ControllerBundle\Tests\Controller\NakimaApiControllerTestCase

## Methods

|             |                |                                                    |  
| ----------- | -------------- | -------------------------------------------------- |  
| protected   | json           | **newUser**([followRedirectBoolean], [args])       |  
| protected   | Void           | **newAnonymous**([followRedirectBoolean])          |  
| protected   | Client         | **popUser**([swapBoolean])                         |  
| protected   | Client         | **getClient**()                                    |  
| protected   | json           | **getUser**()                                      |  
| protected   | json           | **refreshUser**()                                  |  
| protected   | int            | **getStatusCode**()                                |  
| protected   | Void           | **check200**([message])                            |  
| protected   | Void           | **check400**([message])                            |  
| protected   | Void           | **check401**([message])                            |  
| protected   | Void           | **check403**([message])                            |  
| protected   | Void           | **check404**([message])                            |  
| protected   | Void           | **check500**([message])                            |  
| protected   | Void           | **printRaw**()                                     |  
| protected   | Void           | **printJson**()                                    |  
| protected   | json           | **getJson**()                                      |  
| protected   | UploadedFile   | **createFileParam**($folder, $filename)            |  
| protected   | json           | **request**(method, path, [`*`params], [`*`files]) |  
| protected   | json           | **get**(path, [`*`params], [`*`files])             |  
| protected   | json           | **post**(path, [`*`params], [`*`files])            |  
| protected   | json           | **put**(path, [`*`params], [`*`files])             |  
| protected   | json           | **delete**(path, [`*`params], [`*`files])          |  
| protected   | json           | **patch**(path, [`*`params], [`*`files])           |  
| protected   | json           | **head**(path, [`*`params], [`*`files])            |  
| protected   | json           | **options**(path, [`*`params], [`*`files])         |  
| protected   | String         | **rstr**([length])                                 |  
| protected   | String         | **loremWords**([length])                           |  
| protected   | String         | **loremSetences**([length])                        |  
| protected   | String         | **loremParagraphs**([length])                      |  
| _protected_ | String         | **registerUser**([args])                           |  
| _protected_ | json           | **loginUser**([args])                              |  
| _protected_ | json           | **refreshUser**([args])                            |  



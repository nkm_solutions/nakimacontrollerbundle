# Nakima\ControllerBundle\Utils\String

## Methods

|               |         |                                |  
| ------------- | ------- | -------------------------------|  
| public static | String  | **rstr**(length)               |  
| public static | boolean | **startsWith**(string, needle) |  